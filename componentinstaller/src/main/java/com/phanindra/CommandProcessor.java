package com.phanindra;

import java.util.Scanner;

public class CommandProcessor {

    private ComponentManager componentManager;

    public CommandProcessor() {
        this.componentManager = new ComponentManager();
    }

    public static void main(String[] args) {
        CommandProcessor commandProcessor = new CommandProcessor();
        Scanner sc = new Scanner(System.in).useDelimiter("\\s");
        while(sc.hasNextLine()) {
            String input = sc.nextLine().replaceAll("\\s+"," ");
            if(input.equals("END")) {
                sc.close();
                break;
            }
            commandProcessor.processCommand(input);
        }
    }

    private void processCommand(String line) {
        String[] lineTokens = line.split("\\s");
        if(line.startsWith("DEPEND")) {
            componentManager.addDependency(lineTokens[1], lineTokens[2]);
        } else if(line.startsWith("INSTALL")) {
            componentManager.install(lineTokens[1]);
        } else if(line.startsWith("SHOW")) {
            componentManager.show();
        } else if(line.startsWith("REMOVE")) {
            componentManager.remove(lineTokens[1]);
        }
    }
}
