package com.phanindra;

import java.util.*;

public class ComponentManager {

    private ComponentGraph componentGraph;

    private Set<Component> installedComponents;

    public ComponentManager() {
        componentGraph = new ComponentGraph();
        installedComponents = new HashSet<Component>();
    }

    public void addDependency(String first, String second) {
        Component f = new Component(first);
        Component s = new Component(second);
        componentGraph.addEdge(f, s);
    }

    /**
     * Installs the named component by installing its dependencies in topological order.
     * @param componentName
     * @throws CyclicDependencyException if the component cannot be installed since the graph has a cyclic dependency
     */
    public void install(String componentName) throws CyclicDependencyException {
        Component toInstall = new Component(componentName);
        if(installedComponents.contains(toInstall)) {
            return;
        }
        ComponentGraph subgraph = getSubgraph(toInstall);
        List<Component> sorted = subgraph.topologicalSort();
        for(Component c: sorted) {
            if(!installedComponents.contains(c)) {
                System.out.println("installing "+ c);
            } else {
                System.out.println(c + " already installed, skipping");
            }
        }
        installedComponents.addAll(sorted);
    }

    public void remove(String componentName) {
        Component component = new Component(componentName);
        if(!installedComponents.contains(component)) {
            return;
        }
        Set<Component> componentsRequiringGiven = new HashSet<Component>();
        for(Component installedComponent : installedComponents) {
            Set<Component> installedComponentDeps = componentGraph.getDependencies(installedComponent);
            if(installedComponentDeps.contains(component)) {
               componentsRequiringGiven.add(installedComponent);
            }
        }
        if(!componentsRequiringGiven.isEmpty()) {
            System.out.println("Cannot remove "+ component + ", it is required for "+ componentsRequiringGiven);
        } else {
            System.out.println("Removing "+ component);
            installedComponents.remove(component);
        }
        Set<Component> dependencies = componentGraph.getDependencies(component);
        for(Component dependency : dependencies) {
            remove(dependency.getName());
        }
    }

    public void show() {
        System.out.println(componentGraph);
    }

    private ComponentGraph getSubgraph(Component component) {
        Set<Component> allRelated = new HashSet<Component>();
        Queue<Component> q = new ArrayDeque<Component>();
        q.add(component);
        while(!q.isEmpty()) {
            Component s = q.poll();
            if(!allRelated.contains(s)) {
                Set<Component> temp = new HashSet<Component>();
                temp.addAll(componentGraph.getDependencies(s));
                temp.removeAll(allRelated);
                q.addAll(temp);
            }
            allRelated.add(s);
        }
        ComponentGraph subgraph = new ComponentGraph();
        for(Component s: allRelated) {
            Set<Component> temp = componentGraph.getDependencies(s);
            temp.retainAll(allRelated);
            subgraph.addEdges(s, temp);
        }
        return subgraph;
    }

    public Set<Component> getInstalledComponents() {
        return installedComponents;
    }
}
