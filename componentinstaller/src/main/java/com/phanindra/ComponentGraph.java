package com.phanindra;

import java.util.*;

/**
 * Class representing a directed graph of components and their dependencies
 * Uses the adjacency list representation.
 */
public class ComponentGraph {

    private Map<Component, Set<Component>> adjacencyMap;

    public ComponentGraph() {
        adjacencyMap = new HashMap<Component, Set<Component>>();
    }

    public void addEdges(Component component, Set<Component> dependencies) {
        adjacencyMap.put(component, dependencies);
    }

    private void add(Component c) {
        adjacencyMap.put(c, new HashSet<Component>());
    }

    public void addEdge(Component first, Component second) {
        if (!adjacencyMap.containsKey(first)) {
            add(first);
        }
        if (!adjacencyMap.containsKey(second)) {
            add(second);
        }
        // Add the second to the list of first's dependencies
        Set<Component> dependencies = adjacencyMap.get(first);
        dependencies.add(second);
        adjacencyMap.put(first, dependencies);
    }

    public Set<Component> getDependencies(Component c) {
        return adjacencyMap.get(c);
    }

    /**
     * Performs a topological sort using the Kahn algorithm
     * @return The list of components in topological order.
     */
    public List<Component> topologicalSort() throws CyclicDependencyException {
        List<Component> sorted = new ArrayList<Component>();
        Map<Component, Set<Component>> mapCopy = deepCopy(adjacencyMap);
        while(!mapCopy.isEmpty()) {
            Component next = getComponentWithNoDependencies(mapCopy);
            if(next == null) {
                throw new CyclicDependencyException("Cyclic dependency detected");
            }
            sorted.add(next);
            removeFromAllValues(next, mapCopy);
            mapCopy.remove(next);
        }
        return sorted;
    }

    private Map<Component,Set<Component>> deepCopy(Map<Component, Set<Component>> adjacencyMap) {
        Map<Component, Set<Component>> copy = new HashMap<Component, Set<Component>>();
        for(Map.Entry<Component, Set<Component>> entry: adjacencyMap.entrySet()) {
            copy.put(entry.getKey(), new HashSet<Component>(entry.getValue()));
        }
        return copy;
    }

    private Component getComponentWithNoDependencies(Map<Component, Set<Component>> adjacencyMapCopy) {
        for(Map.Entry<Component, Set<Component>> entry: adjacencyMapCopy.entrySet()) {
            if(entry.getValue().isEmpty()) {
                return entry.getKey();
            }
        }
        return null;
    }

    private void removeFromAllValues(Component component, Map<Component, Set<Component>> adjacencyMapCopy) {
        Set<Component> componentsToRemoveFrom = new HashSet<Component>();
        for(Iterator<Map.Entry<Component, Set<Component>>> it = adjacencyMapCopy.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<Component, Set<Component>> entry = it.next();
            if(entry.getValue().contains(component)) {
                componentsToRemoveFrom.add(entry.getKey());
            }
        }
        for(Component c: componentsToRemoveFrom) {
            adjacencyMapCopy.get(c).remove(component);
        }
    }

    @Override
    public String toString() {
        return "ComponentGraph{" +
                "adjacencyMap=" + adjacencyMap +
                '}';
    }
}
