package com.phanindra;

public class CyclicDependencyException extends RuntimeException {
    private String message;

    public CyclicDependencyException(String message) {
        super(message);
    }
}
