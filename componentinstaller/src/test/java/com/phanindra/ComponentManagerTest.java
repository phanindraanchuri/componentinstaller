package com.phanindra;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static java.util.Arrays.asList;
import java.util.HashSet;
import java.util.Set;

public class ComponentManagerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void installAlreadyInstalledComponentsSkipped() {
        ComponentManager manager = new ComponentManager();
        manager.addDependency("A", "B");
        manager.addDependency("A", "C");
        manager.addDependency("B", "C");
        manager.addDependency("D", "E");
        manager.install("B");
        Set<Component> expectedInstalled = new HashSet<Component>(asList(new Component("B"), new Component("C")));
        Set<Component> installed = manager.getInstalledComponents();
        Assert.assertEquals(expectedInstalled, installed);
        manager.install("A");
        Set<Component> expectedInstalledAfter = new HashSet<Component>(asList(new Component("A"), new Component("B"), new Component("C")));
        Assert.assertEquals(expectedInstalledAfter, installed);
    }

    @Test
    public void installCyclicDependencies() {
        thrown.expect(CyclicDependencyException.class);
        ComponentManager manager = new ComponentManager();
        manager.addDependency("A", "B");
        manager.addDependency("B", "C");
        manager.addDependency("C", "A");
        manager.addDependency("D", "E");
        manager.install("A");
    }

    @Test
    public void removeComponentRequiredByInstalledComponent() {
        ComponentManager manager = new ComponentManager();
        manager.addDependency("A", "B");
        manager.addDependency("A", "C");
        manager.addDependency("B", "C");
        manager.install("A");
        Assert.assertEquals(3, manager.getInstalledComponents().size());
        manager.remove("B");
        Assert.assertEquals(3, manager.getInstalledComponents().size());
    }

    @Test
    public void removeComponentTest() {
        ComponentManager manager = new ComponentManager();
        manager.addDependency("A", "C");
        manager.addDependency("B", "C");
        manager.addDependency("B", "D");
        manager.addDependency("C", "D");
        manager.addDependency("D", "E");
        manager.install("A");
        manager.install("B");
        Set<Component> installed = manager.getInstalledComponents();
        Set<Component> expected = new HashSet<Component>(asList(new Component("A"), new Component("B"), new Component("C"), new Component("D"), new Component("E")));
        Assert.assertEquals(expected, installed);
        manager.remove("A");
        Assert.assertTrue(4 == manager.getInstalledComponents().size());
    }
}
